import javax.swing.JOptionPane;
/**
 *Conjetura de Collatz: en donde si es par, este divide el numero entre 2
 * pero si es impar, el numero ingresado se pultiplica por 3, y se le suma 1.
 * @author jose angel juarez ortiz
 * 
 */
import javax.swing.JOptionPane;
public class ConjeturaCollatz {

    public static void main(String[] args) {
        // TODO code application logic here
        int num = Integer.parseInt(JOptionPane.showInputDialog("Digite cualquier numero"));
        ConjeturaCollatz numb = new ConjeturaCollatz();
        System.out.println("Los resultados son:  ");
        
        numb.operacion(num);            
    }
    //metodo que hace la conjetura de Collatz que retorna un valor
    public int operacion(int num){
        System.out.println(num + " ");
        if(num>1){
            if(num%2==0){
               operacion(num/2);
            }else{
                operacion((3*num)+1);                
            }
        }
        return num ;
    }   
}